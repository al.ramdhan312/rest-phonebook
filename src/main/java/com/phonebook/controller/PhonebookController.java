package com.phonebook.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.phonebook.service.PhonebookService;
import com.phonebook.to.PhoneTO;
import com.phonebook.util.Response;

@RestController
@RequestMapping(value = "/api")
public class PhonebookController {
	@Autowired
	private PhonebookService service;
	
	@GetMapping("/v1/get-list-phone")
	public @ResponseBody ResponseEntity<Response<List<PhoneTO>>> getAll() {
		Response<List<PhoneTO>> resp = new Response<List<PhoneTO>>();
		
		resp.setMessage("Berhasil");
		resp.setData(service.getAllPhone());
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(resp);
	}
	
	@PostMapping("/v1/add-phone")
	public @ResponseBody ResponseEntity<Response> addPhone(@RequestBody String data) {
		String result = service.addPhone(new JSONObject(data));
		
		Response<String> resp = new Response<String>();
		
		resp.setMessage("Berhasil simpan !");
		resp.setData(result);
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(resp);
	}
	
	@PostMapping("/v1/delete-phone/{id}")
	public @ResponseBody ResponseEntity<Response> deletePhone(@PathVariable String id) {
		String result = service.deletePhone(id);
		Response<String> resp = new Response<String>();
		
		resp.setMessage("Berhasil hapus !");
		resp.setData(result);
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(resp);
	}
	
	@PostMapping("/v1/update-phone/{id}")
	public @ResponseBody ResponseEntity<Response> updatePhone(@PathVariable String id, @RequestBody String data) {
		String result = service.updatePhone(id, new JSONObject(data));
		Response<String> resp = new Response<String>();
		
		resp.setMessage("Berhasil update !");
		resp.setData(result);
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(resp);
	}
	
	@GetMapping("/v1/phone")
	public @ResponseBody ResponseEntity<Response<List<PhoneTO>>> searchPhone(@RequestParam("search") String key) {
		Response<List<PhoneTO>> resp = new Response<List<PhoneTO>>();
		
		resp.setMessage("Berhasil");
		resp.setData(service.searchPhone(key));
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(resp);
	}
}
