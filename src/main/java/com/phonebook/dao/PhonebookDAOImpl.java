package com.phonebook.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.phonebook.to.PhoneTO;

@Repository
public class PhonebookDAOImpl implements PhonebookDAO {
	@Autowired
	private DataSource datasource;

	@Override
	public List<PhoneTO> getAllPhone() {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer sql = new StringBuffer();
		List<PhoneTO> listResult = new ArrayList<PhoneTO>();
		
		try {
			conn = datasource.getConnection();
			
			sql.delete(0, sql.length());
			sql.append("SELECT * FROM phonebook; \n");
			pst = conn.prepareStatement(sql.toString());
			rst = pst.executeQuery();
			
			while(rst.next()) {
				PhoneTO tobPhone = new PhoneTO();
				
				tobPhone.setPhoneID(rst.getString("PhoneID"));
				tobPhone.setFirstName(rst.getString("FirstName"));
				tobPhone.setLastName(rst.getString("LastName"));
				tobPhone.setPhoneNumber(rst.getString("PhoneNumber"));
				
				listResult.add(tobPhone);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
				pst.close();
				rst.close();
			} catch (SQLException sqlE) {
				sqlE.printStackTrace();
			}
		}
		
		return listResult;
	}

	@Override
	public String addPhone(JSONObject data) {
		Connection conn = null;
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		
		try {
			conn = datasource.getConnection();
			
			sql.delete(0, sql.length());
			sql.append("INSERT INTO phonebook (FirstName, LastName, Address, PhoneNumber) VALUES (?, ?, ?, ?); \n");
			pst = conn.prepareStatement(sql.toString());
			pst.setString(1, data.getString("first-name"));
			pst.setString(2, data.getString("last-name"));
			pst.setString(3, data.getString("address"));
			pst.setString(4, data.getString("phone-number"));
			
			if(pst != null) pst.execute();
		} catch (SQLException sqlE) {
			sqlE.printStackTrace();
			return "0";
		} finally {
			try {
				conn.close();
				pst.close();
			} catch (SQLException sqlE1) {
				sqlE1.printStackTrace();
			}
		}
		
		return "1";
	}

	@Override
	public String deletePhone(String id) {
		Connection conn = null;
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		
		try {
			conn = datasource.getConnection();
			
			sql.delete(0, sql.length());
			sql.append("DELETE FROM phonebook WHERE PhoneID = ?; \n");
			pst = conn.prepareStatement(sql.toString());
			pst.setString(1, id);
			
			if(pst != null) pst.execute();
		} catch (SQLException sqlE) {
			sqlE.printStackTrace();
			return "0";
		} finally {
			try {
				conn.close();
				pst.close();
			} catch (SQLException sqlE1) {
				sqlE1.printStackTrace();
			}
		}
		
		return "1";
	}

	@Override
	public String updatePhone(String id, JSONObject data) {
		Connection conn = null;
		PreparedStatement pst = null;
		StringBuffer sql = new StringBuffer();
		
		try {
			conn = datasource.getConnection();
			
			sql.delete(0, sql.length());
			sql.append("UPDATE phonebook SET FirstName = ?, LastName = ?, Address = ?, PhoneNumber = ? WHERE PhoneID = ?; \n");
			pst = conn.prepareStatement(sql.toString());
			pst.setString(1, data.getString("first-name"));
			pst.setString(2, data.getString("last-name"));
			pst.setString(3, data.getString("address"));
			pst.setString(4, data.getString("phone-number"));
			pst.setString(5, id);
			
			if(pst != null) pst.execute();
		} catch (SQLException sqlE) {
			sqlE.printStackTrace();
			return "0";
		} finally {
			try {
				conn.close();
				pst.close();
			} catch (SQLException sqlE1) {
				sqlE1.printStackTrace();
			}
		}
		
		return "1";
	}

	@Override
	public List<PhoneTO> searchPhone(String key) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer sql = new StringBuffer();
		List<PhoneTO> listResult = new ArrayList<PhoneTO>();
		
		try {
			conn = datasource.getConnection();
			
			sql.delete(0, sql.length());
			sql.append("SELECT * FROM phonebook \n");
			sql.append("WHERE FirstName LIKE '%"+key+"%' OR LastName LIKE '%"+key+"%' OR Address LIKE '%"+key+"%' OR PhoneNumber LIKE '%"+key+"%' \n");
			pst = conn.prepareStatement(sql.toString());
			rst = pst.executeQuery();
			
			while(rst.next()) {
				PhoneTO tobPhone = new PhoneTO();
				
				tobPhone.setPhoneID(rst.getString("PhoneID"));
				tobPhone.setFirstName(rst.getString("FirstName"));
				tobPhone.setLastName(rst.getString("LastName"));
				tobPhone.setPhoneNumber(rst.getString("PhoneNumber"));
				
				listResult.add(tobPhone);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
				pst.close();
				rst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return listResult;
	}
}
