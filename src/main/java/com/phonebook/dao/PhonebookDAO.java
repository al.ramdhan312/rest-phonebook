package com.phonebook.dao;

import java.util.List;

import org.json.JSONObject;

import com.phonebook.to.PhoneTO;

public interface PhonebookDAO {
	public List<PhoneTO> getAllPhone();
	public String addPhone(JSONObject data);
	public String deletePhone(String id);
	public String updatePhone(String id, JSONObject data);
	public List<PhoneTO> searchPhone(String key);
}
