package com.phonebook.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phonebook.dao.PhonebookDAO;
import com.phonebook.to.PhoneTO;

@Service
public class PhonebookServiceImpl implements PhonebookService {
	@Autowired
	private PhonebookDAO dao;
	
	@Override
	public List<PhoneTO> getAllPhone() {
		return dao.getAllPhone();
	}

	@Override
	public String addPhone(JSONObject data) {
		return dao.addPhone(data);
	}

	@Override
	public String deletePhone(String id) {
		return dao.deletePhone(id);
	}

	@Override
	public String updatePhone(String id, JSONObject data) {
		return dao.updatePhone(id, data);
	}

	@Override
	public List<PhoneTO> searchPhone(String key) {
		return dao.searchPhone(key);
	}
}
