package com.phonebook.service;

import java.util.List;

import org.json.JSONObject;

import com.phonebook.to.PhoneTO;

public interface PhonebookService {
	public List<PhoneTO> getAllPhone();
	public String addPhone(JSONObject data);
	public String deletePhone(String id);
	public String updatePhone(String id, JSONObject data);
	public List<PhoneTO> searchPhone(String key);
}
