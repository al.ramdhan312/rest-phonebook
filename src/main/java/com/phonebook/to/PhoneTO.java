package com.phonebook.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneTO {
	public String PhoneID;
	public String FirstName;
	public String LastName;
	public String PhoneNumber;
}
